import React, { useState } from 'react';
import { getError, getOption } from '../../utils/formValidation';

const Form = ({ children, onSubmit }) => {
	const getInitialState = () => {
		let values = {};
		let errorList = {};
		let isTouched = {};

		React.Children.forEach(children, child => {
			switch (child.type.name) {
				case 'Select':
					let options = {};
					child.props.options.forEach(option => {
						options[option] = false;
					});
					values[child.props.name] = options;
					errorList[child.props.name] = '';
					isTouched[child.props.name] = false;
					break;
				case 'Input':
				case 'Password':
					values[child.props.name] = '';
					errorList[child.props.name] = '';
					isTouched[child.props.name] = false;
					break;
			}
		})

		return {
			values,
			errorList,
			isTouched,
		};
	}

	const [state, setState] = useState(getInitialState());

	const handleChange = ({ name, value }) => {
		const newState = {
			...state,
			values: {
				...state.values,
				[name]: value,
			},
			errorList: {
				...state.errorList,
				[name]: getError({
					name,
					value,
					isTouched: true,
				}),
			},
			isTouched: {
				...state.isTouched,
				[name]: true,
			},
		};

		setState(newState);
	};

	const handleSubmit = () => {
		const errorList = {};

		Object.keys(state.values).forEach((item, index) => {
			errorList[item] = getError({
				name: item,
				value: Object.values(state.values)[index],
				isTouched: state.isTouched[item],
				password: (item === 'confirmPassword') && state.values.password,
			});
		});

		setState({
			...state,
			errorList,
		});

		!Object.values(errorList).filter(item => item !== '').length && onSubmit(state.values)
	};

	const getProps = (type, value) => {
		switch (type) {
			case 'Select':
				return {
					name: value,
					error: state.errorList[value],
					options: state.values[value],
					onSelect: (e) => handleChange({
						name: value,
						value: (getOption(e).index === 0) ? getInitialState().values[value] : {
							...getInitialState().values[value],
							[`${getOption(e).name}`]: true,
						},
					}),
				}
			case 'Input':
			case 'Password':
				return {
					name: value,
					type: (value === 'email') ? 'email' : 'text',
					value: state.values[value],
					error: state.errorList[value],
					placeholder: value[0].toUpperCase() + value.substring(1),
					changeInputValue: (e) => handleChange(e.target),
					validation: (e) => setState({
						...state,
						errorList: {
							...state.errorList,
							[e.target.name]: getError({
								name: e.target.name,
								value: e.target.value,
								isTouched: state.isTouched[e.target.name],
								password: (value === 'confirmPassword') && state.values.password,
							}),
						},
					})
				}
			case 'Button':
				return {
					onSubmit: () => handleSubmit(),
				}
		}
	}

	return (
		<div className="app">
			{React.Children.map(children, child => React.cloneElement(child, { ...getProps(child.type.name, child.props.name) }))}
		</div>
	);
};

export default Form;