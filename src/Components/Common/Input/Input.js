import React from 'react';
import './Input.css';

const Input = ({
	name, type, placeholder, value, error, autofocus, maxlength, changeInputValue, validation,
}) => (
	<div className="form">
		{error && (
			<div className="error">
				{error}
			</div>
		)}
		<input
			className="input"
			name={name}
			type={type}
			placeholder={placeholder}
			value={value}
			autoFocus={autofocus}
			maxLength={maxlength}
			onChange={(e) => changeInputValue(e)}
			onBlur={(e) => validation(e)}
		/>
	</div>
);

export default Input;
