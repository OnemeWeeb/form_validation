import React from 'react';

const Select = ({
	name, error, options, onSelect,
}) => (
	<div className="select">
		{error && <div className="error">{error}</div>}
		<select onBlur={(e) => onSelect(e)}>
			<option selected={!(Object.values(options).includes(true))}>
				{name[0].toUpperCase() + name.substring(1)}
			</option>
			{Object.keys(options).map((item, index) => <option key={index} value={item} selected={options[item]}>
					{item[0].toUpperCase() + item.substring(1)}
				</option>)}
		</select>
	</div>
);

export default Select;