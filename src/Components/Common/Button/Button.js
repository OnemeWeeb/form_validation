import React from 'react';

const Button = ({ name, onSubmit }) => {
  return (
    <div className="button" onClick={() => onSubmit()}>
			{name}
    </div>
  );
}

export default Button;