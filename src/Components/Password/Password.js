import React, { useState } from 'react';
import Input from '../Common/Input/Input';

const Password = ({ name, placeholder, value, error, changeInputValue, validation }) => {
  const initialState = {
    isShowing: false,
  }

  const [state, setState] = useState(initialState);

  return (
    <div className="form">
      <Input
      name={name}  
      type={state.isShowing ? 'text' : 'password'}
      placeholder={placeholder}
      value={value}
      error={error}
      maxlength={30}
      changeInputValue={(e) => changeInputValue(e)}
      validation={(e) => validation(e)}
      />
      <div className="button" onClick={() => setState({ isShowing: !state.isShowing })}>
        {state.isShowing ? 'скрыть' : 'показать'}
      </div>
    </div>
  );
}

export default Password;