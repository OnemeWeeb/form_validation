import React from 'react';

import Form from './Components/Form/Form';
import Password from './Components/Password/Password';
import Input from './Components/Common/Input/Input';
import Select from './Components/Common/Select/Select';
import Button from './Components/Common/Button/Button';

import './App.css';

const App = () => {
	const onSubmit = (value) => {
		console.log(value);
	}
	return (
		<div className="app">
			<Form onSubmit={(value) => onSubmit(value)}>
				<Input name='email' autofocus />
				<Password name='password' />
				<Password name='confirmPassword' />
				<Input name='nickname' />
				<Select name='gender' options={['male', 'female']}/>
				<Button name='Save'/>
			</Form>
		</div>
	);
};

export default App;


