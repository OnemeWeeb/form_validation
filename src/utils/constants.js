export const fields = [
	{
		name: 'email',
		type: 'input',
		value: '',
	},
	{
		name: 'password',
		type: 'input',
		value: '',
	},
	{
		name: 'confirmPassword',
		type: 'input',
		value: '',
	},
	{
		name: 'nickname',
		type: 'input',
		value: '',
	},
	{
		name: 'gender',
		type: 'select',
		options: ['male' , 'female']
	}
]