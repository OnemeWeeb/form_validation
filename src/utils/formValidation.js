export const getError = ({
	name, value, isTouched, password,
}) => {
	let error = '';

	if (value === '') {
		error = 'Поле не заполнено!';
	} else {
		switch (name) {
		case 'password':
			if (value.length < 6) {
				error = isTouched ? 'Пароль должен содержать более 6-ти символов!' : '';
			}
			break;
		case 'confirmPassword':
			if (value !== password) {
				error = isTouched ? 'Введённые пароли не совпадают!' : '';
			}
			break;
		case 'nickname':
			if (/^[a-zA-Z0-9\s]+$/.test(value) === false) {
				error = isTouched ? 'Имя пользователя может содержать только латиницу!' : '';
			}
			break;
		case 'email':
			if (/^\w+@[a-zA-Z_]+?.[a-zA-Z]{2,3}$/.test(value) === false) {
				error = isTouched ? 'Неправильный формат email!' : '';
			}
			break;
		case 'gender':
			if (!Object.values(value).filter(item => item === true).length) {
				error = 'Пол не выбран!';
			}
			break;
		}
	}

	return error;
};

export const getOption = (e) => ({
	index: e.target.options.selectedIndex,
	name: e.target.options[e.target.options.selectedIndex].value,
});
